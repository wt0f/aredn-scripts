AAMM API
========

The `aamm.lua` file in the `cgi-bin` directory provides a simple API
used by the front end web page. It is also possible to use this API
for automated alerts.

At this time only `GET` calls have been validated as functioning and
returning correct information. In the future it is expected to move the
API calls to `POST` calls.

Each call requires an `action` parameter to specify the API call to
service. Most calls will require additional parameters as specified
below.

Each API call returns a JSON object with two primary keys: `result` and
`data`. The `result` key will have "OK" for successful calls and "NAK"
for failures. The `data` key will vary according to the call and is
detailed in each API section.

Upon a API failure (i.e. "NAK") the `data` key will have one subkey named
`result` that will have a string describing the failure.

settings API
------------

Returns the configuration for the AAMM front end.

### API Parameters

| Parameter |     Value     | Notes |
|-----------|---------------|-------|
| action    | settings      |       |

### data Key Results

JSON document with the following keys:

* `api_url`
* `protocol_version`
* `app_version`
* `debug`
* `alert_groups`

check_credentials API
---------------------

This call allows the front end to authenticate the user. It will return
a simple "OK" or "NAK" in `result` if the supplied password matches the
configuration.

### API Parameters

| Parameter |     Value         | Notes              |
|-----------|-------------------|--------------------|
| action    | check_credentials |                    |
| auth      |                   | Plaintext password |

### data Key Results

None.

list_alerts API
---------------

Generate a list of current AAM messages.

### API Parameters

| Parameter |     Value     | Notes |
|-----------|---------------|-------|
| action    | list_alerts   |       |

### data Key Results

JSON document consisting of an array of maps. Each map represents an
alert and has the following keys:

* `file`
* `modified_at`
* `size`

list_nodes API
--------------

Generate a list of current nodes seen on the network. This list is
produced through the OLSR daemon. If a new node appears on the network
there may be a delay before it is listed as a node.

### API Parameters

| Parameter |     Value     | Notes |
|-----------|---------------|-------|
| action    | list_nodes    |       |

### data Key Results

JSON document consisting of an array of node names.

create_alert API
----------------

This is the origin of all AAM messages (unless one is created manually).
The specified text simply gets written to the file that has been
specified.

Failures of the `create_alert` API are generally permissions related. If
a failure does occur, then one should check the ownership and permissions
of the directory for the `aam_dir` variable in the configuration.

### API Parameters

| Parameter  |     Value     | Notes                                           |
|------------|---------------|-------------------------------------------------|
| action     | create_alert  |                                                 |
| auth       |               | Plaintext password                              |
| alert_file |               | Filename of AAM (i.e. `all.txt`)                |
| alert_text |               | Contents of AAM. See note about quoting values. |

### data Key Results

None.

get_alert API
-------------

This retrieves a single AAM and returns the contents of the file.

### API Parameters

| Parameter  |     Value     | Notes                                           |
|------------|---------------|-------------------------------------------------|
| action     | get_alert     |                                                 |
| alert_file |               | Filename of AAM (i.e. `all.txt`)                |

### data Key Results

JSON document consisting of the following keys:

* `message`
* `modified_at`
* `size`

update_alert API
----------------

Replace the contents of an AAM. Only the contents of the AAM is updated.
The filename used by the AAM stays the same.

### API Parameters

| Parameter  |     Value     | Notes                                           |
|------------|---------------|-------------------------------------------------|
| action     | update_alert  |                                                 |
| auth       |               | Plaintext password                              |
| alert_file |               | Filename of AAM (i.e. `all.txt`)                |
| alert_text |               | Contents of AAM. See note about quoting values. |

### data Key Results

None.

delete_alert API
----------------

Remove an AAM file from the AAM directory.

### API Parameters

| Parameter  |     Value     | Notes                                           |
|------------|---------------|-------------------------------------------------|
| action     | delete_alert  |                                                 |
| auth       |               | Plaintext password                              |
| alert_file |               | Filename of AAM (i.e. `all.txt`)                |

### data Key Results

None.
