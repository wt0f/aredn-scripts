Configuration
=============

Configuration of AAM Manager is controlled by the `aamm_config.lua` file
that is usually located in the web server's `cgi-bin`. For an AREDN node
installation this location would be `/www/cgi-bin`. For other
installations, please consult the web server documentation for the
location of the `cgi-bin` directory.

The configuration consists of a standard LUA table object named `Config`.
The following table keys are recognized.

|     Key    |   Default   |   Description                                 |
|------------|-------------|-----------------------------------------------|
| aam_dir    | /www/aam    | The directory on the web server to store AAMs |
| api_url    | /cgi-bin/aamm.lua | The URL path to the `aamm.lua` script   |
| password   |             | The administrator password used to manage AAMs |
| debug      | false       | Used to control debugging messages in the browser console |
| lock_file  | /www/aam/lock | Lock file location to prevent simultaneous saves of the same AAM |
| alert_groups | { }       | List of AAM group names for generating group AAMs |

