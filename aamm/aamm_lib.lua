--

local json     = require("luci.jsonc")

function json_page(data)
    print("Content-type: application/json\n\n")

    print(json.stringify(data))
    -- output:flush()
end

function text_page(data)
    print("Content-type: text/plain\n\n")

    print(data)
end

function api_result(code, data)
    data = data or {}

    json_page({
        result = code,
        data = data
    })
end

function aam_file_path(alert_file)
    return Config.aam_dir .. "/" .. string.lower(alert_file)
end

local lock_fd
function get_lock(lock_file)
    if not lock_fd then
        lock_fd = nixio.open(lock_file, "w", "666")
    end
    lock_fd:lock("lock")
end

function release_lock(lock_file)
    lock_fd:lock("ulock")
    nixio.fs.remove(lock_file)
end

function string.unquote(text)
    local escapes = { ["#32;"] = " ",
                      ["#43;"] = "+", }

    for match, repl in pairs(escapes) do
        text = string.gsub(text, match, repl)
    end

    return text
end
