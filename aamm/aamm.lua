#!/usr/bin/env lua

package.path = package.path .. ";/www/cgi-bin/?.lua;/etc/?.lua;../lib/?.lua"

require("nixio.fs")
require('luci.http')
-- local http     = require("socket.http")
-- local https    = require("ssl.https")
-- local argparse = require("argparse")
require("aamm_lib")
local config   = require("aamm_config")


APP_VERSION = "main"
PROTOCOL_VERSION = "1.0"

function debug(msg)
    if config.debug then
        print(msg)
    end
end

local function dumpTable( t )

    local dumpTable_cache = {}
    local dumpString = ""

    local function sub_dumpTable( t, indent )
        local dumpString = ""

        if ( dumpTable_cache[tostring(t)] ) then
            dumpString = dumpString .. indent .. "*" .. tostring(t)
        else
            dumpTable_cache[tostring(t)] = true
            if ( type( t ) == "table" ) then
                for pos,val in pairs( t ) do
                    if ( type(val) == "table" ) then
                        dumpString = dumpString .. indent .. "[" .. pos .. "] => " .. tostring( t ).. " {\n"
                        dumpString = dumpString .. sub_dumpTable( val, indent .. string.rep( " ", string.len(pos)+8 ) ) .. "\n"
                        dumpString = dumpString .. indent .. string.rep( " ", string.len(pos)+6 ) .. "}\n"
                    elseif ( type(val) == "string" ) then
                        dumpString = dumpString .. indent .. "[" .. pos .. '] => "' .. val .. '"' .. "\n"
                    else
                        dumpString = dumpString .. indent .. "[" .. pos .. "] => " .. tostring(val) .. "\n"
                    end
                end
            else
                dumpString = dumpString .. indent..tostring(t) .. "\n"
            end
        end
        return dumpString
    end

    if ( type(t) == "table" ) then
        dumpString = dumpString .. tostring(t) .. " {\n"
        dumpString = dumpString .. sub_dumpTable( t, "  " ) .. "\n"
        dumpString = dumpString .. "}\n"
    else
        dumpString = dumpString .. sub_dumpTable( t, "  " ) .. "\n|"
    end

    return dumpString
end

local query = { }
local uploadfilename
if os.getenv("QUERY_STRING") ~= "" or os.getenv("REQUEST_METHOD") == "POST" then
    local request = luci.http.Request(nixio.getenv(),
        function()
            local v = io.read(1024)
            if not v then
                io.close()
            end
            return v
        end
    )
    local fp
    request:setfilehandler(
        function(meta, chunk, eof)
            if not fp then
                if meta and meta.file then
                    uploadfilename = meta.file
                end
                nixio.fs.mkdir(tmp_upload_dir)
                fp = io.open(tmp_upload_dir .. "/file", "w")
            end
            if chunk then
                fp:write(chunk)
            end
            if eof then
                fp:close()
            end
        end
    )
    query = request:formvalue()
end

function error(msg)
    text_page(msg)
end

function settings()
    values = {
        api_url = Config.api_url,
        app_version = APP_VERSION,
        protocol_version = PROTOCOL_VERSION,
        debug = Config.debug,
        alert_groups = Config.alert_groups,
    }

    api_result("OK", values)
end

function check_credentials(auth)
    if auth == Config.password then
        api_result("OK")
    else
        api_result("NAK")
    end
end

function list_alerts(dir)
    local alerts = {}

    for alert_file in nixio.fs.dir(dir)
    do
        if string.match(alert_file, "\.txt$") then
            local stat = nixio.fs.stat(aam_file_path(alert_file))
            alerts[#alerts + 1] = {
                file = alert_file,
                modified_at = stat.mtime,
                size = stat.size,
            }
        end
    end

    api_result("OK", alerts)
end

function list_nodes()
    local nodes = {}
    local node_match = "%d+\.%d+\.%d+\.%d+%s+dtdlink\.([%a%d-_]+)\.local\.mesh%s"

    for line in io.lines("/var/run/hosts_olsr")
    do
        local node_name = string.match(line, node_match)
        if node_name then
            table.insert(nodes, node_name)
        end
    end

    table.sort(nodes, function (l, r)
        return string.lower(l) < string.lower(r)
    end)
    api_result("OK", nodes)
end

function read_file(file)
    local text = ""
    local fp = io.input(file)
    repeat
        local line = fp:read ("*l")
        if line then
            text = text .. line .. "\n"
        end
    until not line
    fp:close ()

    return text
end

function get_alert(file)
    local message = read_file(aam_file_path(file))
    local stat = nixio.fs.stat(aam_file_path(file))

    api_result("OK", {
        message = message,
        modified_at = stat.mtime,
        size = stat.size,
    })
end

function _delete_alert(file)
    nixio.fs.remove(aam_file_path(file))

    if nixio.fs.stat(aam_file_path(file)) then
        return false
    else
        return true
    end
end

function delete_alert(file)
    if _delete_alert(file) then
        api_result("OK", { result = "Alert deleted: " .. file })
    else
        api_result("NAK", { result = "Alert deletion failed: " .. file })
    end
end

function _create_alert(file, message)
    get_lock(Config.lock_file)

    local fp = io.open(aam_file_path(file), "w")
    if not fp then
        release_lock(Config.lock_file)
        error("Cannot create alert")
        return false
    end
    fp:write(string.unquote(message))
    fp:close()

    release_lock(Config.lock_file)
    return true
end

function create_alert(file, message)
    if _create_alert(file, message) then
        api_result("OK", { result = "Alert created: " .. file })
    else
        api_result("NAK", { result = "Alert creation failed: " .. file })
    end
end

-- query = { action = "list_alerts" }
-- query = { action = "list_nodes" }
-- query = { action = "get_alert", alert_file = "all.txt" }
-- query = { action = "create_alert", alert_file = "test1.txt", alert_text = "abcdefg", auth = "foobar" }
-- query = { action = "delete_alert", alert_file = "test1.txt", auth = "foobar" }
-- query = { action = "update_alert", alert_file = "all.txt", auth = "foobar", alert_text = "testing foobar" }

if query.action == "settings" then
    settings()
elseif query.action == "list_alerts" then
    list_alerts(Config.aam_dir)
elseif query.action == "list_nodes" then
    list_nodes()
elseif query.action == "get_alert" then
    get_alert(query.alert_file)
elseif query.action == "check_credentials" then
    check_credentials(query.auth)
elseif query.action == "create_alert" and query.auth then
    -- validate password
    create_alert(query.alert_file, query.alert_text)

--        error("no password specified for restricted function")
elseif query.action == "delete_alert" and query.auth then
    -- validate password
    delete_alert(query.alert_file)
elseif query.action == "update_alert" and query.auth then
    -- validate password
    if _delete_alert(query.alert_file) then
        if _create_alert(query.alert_file, query.alert_text) then
            api_result("OK", { result = "Alert updated: " .. query.alert_file })
        end
    else
        api_result("NAK", { result = "Alert update failed: " .. query.alert_file })
    end
else
    error("error no action")
end
