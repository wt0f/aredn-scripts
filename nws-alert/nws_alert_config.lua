Config = {
    alerts = {
        FLC127 = 'WX-Volusia',
        FLC035 = 'WX-Flagler',
        FLC069 = 'WX-Lake',
        FLC009 = 'WX-Brevard',
    },

    debug = false,

    sender = "WX_Alert_Bot",
    meshchat_url = 'http://localnode.local.mesh:8080/cgi-bin/meshchat',
    notification_file = '/tmp/nws-alerts-notifications',

    post_type = 'headline',     -- headline, details

    -- NWS_API_URL = "https://api.weather.gov/alerts/active?status=actual&message_type=%TYPE%&zone=%ZONE%&limit=10"
    nws_api_url = "https://api.weather.gov/alerts/active/zone/%ZONE%",
}

return Config
