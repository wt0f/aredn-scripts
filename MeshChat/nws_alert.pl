#!/usr/bin/env perl

# Setup to be able to find the config and library
BEGIN { push @INC, '/www/cgi-bin', '/usr/lib/cgi-bin', '.' }
use nws_alert_config;
use json_tiny;
use Data::Dumper;

# $NWS_API_URL = "https://api.weather.gov/alerts/active?status=actual&message_type=%TYPE%&zone=%ZONE%&limit=10";
$NWS_API_URL = "https://api.weather.gov/alerts/active/zone/%ZONE%";

sub debug {
    if ($DEBUG) {
        $msg = shift;
        print "$msg\n";
    }
}

sub get_alerts {
    my $zone = shift;
    my $type = shift;

    my $url = $NWS_API_URL;
    $url =~ s/%ZONE%/$zone/;
    $url =~ s/%TYPE%/$type/;

    debug("URL = $url");
    open($fd, "curl -so - '$url' |");
    $text = join('', <$fd>);
    close($fd);

    JSON::Tiny::decode_json($text);
}

=pod
B<send_to_meshchat($channel, $text)>
=cut
sub send_to_meshchat {
    my $channel = shift;
    my $text = shift;
    my $alert_file = "/tmp/nws-alert-$$.txt";

    open($fd, ">$alert_file");
    print $fd $text;
    close($fd);

    my $cmd = "curl -s --data-urlencode action=send_message
               --data-urlencode 'message@$alert_file'
               --data-urlencode 'channel=$channel'
               --data-urlencode 'call_sign=$SENDER' $MESHCHAT_URL";
    debug("curl cmd=$cmd");
    my $result = qx($cmd);
    debug("result=$result");

    unlink($alert_file);
}

=pod
B<load_notifications($file)> Hash<ID, time>
=cut
sub load_notifications {
    my $file = shift;
    my %notifications;

    open($fd, $file);
    for my $entry (<$fd>) {
        chomp $entry;
        ($id, $time) = split('@', $entry);
        $notifications{$id} = $time;
    }
    close($fd);

    %notifications;
}

=pod
B<save_notifications($file, %notifications)>
=cut
sub save_notifications {
    my $file = shift;
    my $notifications = shift;

    open($fd, ">$file");
    for my $id (keys %$notifications) {
        $fd->write($id . '@' . $notifications->{$id} . "\n");
    };
    close($fd);
}

=pod
B<notification_seen($id)> Bool
=cut
sub notification_seen {
    my $id = shift;
    $NOTIFICATIONS{$id};
}

=pod
B<prune_notifications($notifications)> Hash<id, time>
=cut
sub prune_notifications {
    my $notifs = shift;
    my %current_notifs;
    my $prune_time = time() - 86400;    # 24 hours ago

    # copy only notification that have occured in the past 24 hours
    for my $id (keys %$notifs) {
        if ($notifs->{$id} > $prune_time) {
            $current_notifs{$id} = $notifs->{$id};
        }
    }

    %current_notifs;
}

=pod
B<create_post($headline, $body)> string
=cut
sub create_post {
    my $hl = shift;
    my $body = shift;

    $body =~ s/(?!\n)\n/ /;

    "$hl\n\n$body";
}

our %NOTIFICATIONS = load_notifications($NOTIFICATION_FILE);
debug("load_notifications = " . Dumper(\%NOTIFICATIONS));

# Pull the alerts for each zone
for my $zone_code (keys %ALERTS) {
    debug("Processing $zone_code");
    $obj = get_alerts($zone_code, 'alert,update');

    if (scalar @{$obj->{'features'}}) {
        for my $event (@{$obj->{'features'}}) {
            my $id = $event->{'properties'}{'id'};

            if (notification_seen($id) == undef) {
                my $channel = $ALERTS{$zone_code};
                my $headline = $obj->{'features'}[0]{'properties'}{'headline'};
                my $body =  $obj->{'features'}[0]{'properties'}{'description'};

                my $text = create_post($headline, $body);

                debug("Sending $id to channel $channel");
                send_to_meshchat($channel, $text);

                # Add to notifications
                $NOTIFICATIONS{$id} = time();

            }
        }
    }
}

%NOTIFICATIONS = prune_notifications(\%NOTIFICATIONS);
debug("save_notifications = " . Dumper(\%NOTIFICATIONS));
save_notifications($NOTIFICATION_FILE, \%NOTIFICATIONS);