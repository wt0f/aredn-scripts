# AREDN Scripts

This repo contains a number of scripts and files that are used to
manage or enhance a AREDN node.

## MeshChat scripts

* `nws_alert.pl`: Script to retrieve the current alerts from the National
  Weather Service and post the alerts to specific channels.

* `nws_alert.lua`: Script to retrieve the current alerts from the National
  Weather Service and post the alerts to specific channels. Need to have
  libopenssl and luasec packages installed from https://downloads.openwrt.org/releases/packages-18.06. This will add about 2MB to the filesystem.

## AAM Manager

Web application to manage AREDN alert messages. Allows the management of 
AAMs from web browser instead of having to log into a node and edit files.
